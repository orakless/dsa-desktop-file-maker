"""
    Hello! This script is a .desktop-file maker for DummySteamAPI. This script
    checks which games currently supported by DSA you have in your Steam
    library, and add a .desktop entry for these.
"""

import re
import os
import shutil

# Change these with your paths. You can add multiple paths for STEAM_DIR.
HOME = os.getenv("HOME")
DSA_DESKTOP_DIR = f"{HOME}/.local/share/applications/DSA"
STEAM_DIR = [f"{HOME}/.steam/steam/steamapps/common/"]
DSA_DIR = f"{HOME}/Games/DummySteamAPI/"

STEAM_GAMES = []
DSA_COVERED_GAMES = []
BOTH = []


def make_desktop_file(game):
    """"It makes the .desktop file for {game}"""
    file_content = f"[Desktop Entry]\n" \
                   f"Encoding=UTF-8\n" \
                   f"Name={game['real_name']} ({game['arch']})\n" \
                   f"Comment=Play this game on DSA\n" \
                   f"Exec=sh '{DSA_DIR}{game['file_name']}'\n" \
                   f"Type=Application\n" \
                   f"Categories=Game;DSA"
    return file_content


def match_games():
    """
        This function checks which games do you have in your game library
        that are officially compatible with DummySteamAPI
    """
    for path in STEAM_DIR:
        for file in os.listdir(path):
            STEAM_GAMES.append({
                "name": re.sub(r"[^a-zA-Z0-9]", "", file).lower(),
                "real_name": file})

    for file in os.listdir(DSA_DIR):
        if re.search(r"launch-(\w*)-(\w*).sh", file) is not None:
            DSA_COVERED_GAMES.append(
                {"name": re.sub(r"launch-(\w*)-(\w*).sh", r"\1", file).lower(),
                 "file_name": file,
                 "arch": re.sub(r"launch-(\w*)-(\w*).sh", r"\2", file)})

    for dsa_game in DSA_COVERED_GAMES:
        for item in STEAM_GAMES:
            if dsa_game["name"] != item["name"]:
                continue
            BOTH.append({
                "name": dsa_game["name"],
                "real_name": item["real_name"],
                "file_name": dsa_game["file_name"],
                "arch": dsa_game["arch"]
            })


match_games()

if os.path.isdir(DSA_DESKTOP_DIR):
    shutil.rmtree(DSA_DESKTOP_DIR)

os.mkdir(DSA_DESKTOP_DIR)
for complete_game in BOTH:
    with open(f"{DSA_DESKTOP_DIR}/{complete_game['name']}"
              f"-{complete_game['arch']}.desktop", "w") as desktop_file:
        desktop_file.write(make_desktop_file(complete_game))
        print(f"I did \"{complete_game['real_name']}\" for arch "
              f"{complete_game['arch']}")
